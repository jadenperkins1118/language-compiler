# Language Compiler #

This repository has several packages and APIs for creating language compilers and also contains a virtual machine to compile and run languages created with the API
Feel free to clone or fork the repo and explore the code for your own curosity and education on the subjects

### What is this repository for? ###

* Exploring compilers and virtual machines

### How do I get set up? ###

* Clone or fork the repo

### Contribution guidelines ###

* This repository is an open source exploration of language compilers and virtual machines
* Contributions are not necessary, but are welcome

### Who do I talk to? ###

* Repo owner or admin
package com

import java.lang.Exception

inline fun <R> attempt(block: () -> R?) = try { block() } catch (e: Exception) { null }

fun parseValue(value: String): Any? {
    if (value == "null") return null
    if (value == "true") return 1
    if (value == "false") return 0
    if (value.endsWith("L")) attempt { value.replace("L", "").toLong() }?.let { return it }
    if (value.endsWith("F")) attempt { value.replace("F", "").toFloat() }?.let { return it }
    if (value.endsWith("D")) attempt { value.replace("D", "").toDouble() }?.let { return it }
    attempt { value.toInt() }?.let { return it }
    return value
}

private inline fun <T> T.concatString(size: Int, type: String, getString: T.() -> String): String {
    return if (size > 20) "$type[$size elements]" else getString()
}

fun Any?.asString(): String {
    return when (this) {
        is BooleanArray -> concatString<BooleanArray>(size, "B", BooleanArray::contentToString)
        is ByteArray -> concatString<ByteArray>(size, "b", ByteArray::contentToString)
        is CharArray -> concatString<CharArray>(size, "C", CharArray::contentToString)
        is ShortArray -> concatString<ShortArray>(size, "S", ShortArray::contentToString)
        is IntArray -> concatString<IntArray>(size, "I", IntArray::contentToString)
        is LongArray -> concatString<LongArray>(size, "L", LongArray::contentToString)
        is FloatArray -> concatString<FloatArray>(size, "F", FloatArray::contentToString)
        is DoubleArray -> concatString<DoubleArray>(size, "D", DoubleArray::contentToString)
        is Array<*> -> concatString<Array<*>>(size, "Object", Array<*>::contentDeepToString)
        else -> toString()
    }
}

inline fun <R> time(block: () -> R): Pair<R, Long> {
    val start = System.nanoTime()
    val result = block()
    val end = System.nanoTime()
    return result to (end - start)
}
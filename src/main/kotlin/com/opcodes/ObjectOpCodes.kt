package com.opcodes

import com.vm.ExecutionContext
import com.vm.ScriptObject
import com.vm.VMFunction
import com.vm.VMObject
import com.vm.instruction.BasicInstruction
import com.vm.instruction.Instructions
import kotlin.reflect.KFunction
import kotlin.reflect.KProperty

fun registerObjectInstructions() {
    Instructions.register("new") { NEW() }

    Instructions.register("get") { GET(it[1]) }
    Instructions.register("getdynamic") { GETDYNAMIC() }

    Instructions.register("put") { PUT(it[1]) }
    Instructions.register("putdynamic") { PUTDYNAMIC() }

    Instructions.register("has") { HAS(it[1]) }
    Instructions.register("hasdynamic") { HASDYNAMIC() }

    Instructions.register("del") { DEL(it[1]) }
    Instructions.register("deldynamic") { DELDYNAMIC() }

    Instructions.register("concat") { CONCAT() }
    Instructions.register("tostring") { TOSTRING() }
    Instructions.register("tochars") { TOCHARS() }
}

fun VMFunction.NEW() = instruct(NEW)
fun VMFunction.GET(fieldName: String) = instruct(BasicInstruction { it.getField(fieldName) })
fun VMFunction.GETDYNAMIC() = instruct(GETDYNAMIC)
fun VMFunction.PUT(fieldName: String) = instruct(BasicInstruction { it.putField(fieldName) })
fun VMFunction.PUTDYNAMIC() = instruct(PUTDYNAMIC)
fun VMFunction.HAS(fieldName: String) = instruct(BasicInstruction { it.hasField(fieldName) })
fun VMFunction.HASDYNAMIC() = instruct(HASDYNAMIC)
fun VMFunction.DEL(fieldName: String) = instruct(BasicInstruction { it.delField(fieldName) })
fun VMFunction.DELDYNAMIC() = instruct(DELDYNAMIC)

fun VMFunction.CONCAT() = instruct(CONCAT)
fun VMFunction.TOSTRING() = instruct(TOSTRING)
fun VMFunction.TOCHARS() = instruct(TOCHARS)

private val NEW = BasicInstruction { it.push(VMObject()) }
private val GETDYNAMIC = BasicInstruction { it.getField(it.pop() as String) }
private val PUTDYNAMIC = BasicInstruction { it.putField(it.pop() as String) }
private val HASDYNAMIC = BasicInstruction { it.hasField(it.pop() as String) }
private val DELDYNAMIC = BasicInstruction { it.delField(it.pop() as String) }

private val CONCAT = BasicInstruction {
    val rhs = it.pop().toString()
    it.push(it.pop().toString() + rhs)
}
private val TOSTRING = BasicInstruction { it.push(it.pop().toString()) }
private val TOCHARS = BasicInstruction { it.push(it.pop().toString().toCharArray()) }

private fun ExecutionContext.getField(fieldName: String) {
    val obj = pop() as ScriptObject
    push(obj.getMember(this, fieldName))
}
private fun ExecutionContext.putField(fieldName: String) {
    val value = pop()
    (pop() as VMObject)[fieldName] = value
}
private fun ExecutionContext.hasField(fieldName: String) { push((pop() as VMObject).contains(fieldName)) }
private fun ExecutionContext.delField(fieldName: String) { (pop() as VMObject).delField(fieldName) }
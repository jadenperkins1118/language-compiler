package com.opcodes

enum class ArrayType(val create: (Int) -> Any) {
    T_BOOLEAN(::BooleanArray),
    T_CHAR(::CharArray),
    T_FLOAT(::FloatArray),
    T_DOUBLE(::DoubleArray),
    T_BYTE(::ByteArray),
    T_SHORT(::ShortArray),
    T_INT(::IntArray),
    T_LONG(::LongArray),
    T_OBJECT({ arrayOfNulls<Any?>(it) })
}
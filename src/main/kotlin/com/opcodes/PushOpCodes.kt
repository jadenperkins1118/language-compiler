package com.opcodes

import com.parseValue
import com.vm.VMFunction
import com.vm.instruction.BasicInstruction
import com.vm.instruction.Instructions

fun registerPushInstructions() {
    Instructions.register("push") { PUSH(parseValue(it.getOrElse(1) { "" })) }
}

/**Optimization to pick correct sequence of instructions to push values onto the stack*/
/*fun VMMethod.PUSH(value: Any?) {
    val instr = when (value) {
        null -> arrayOf(ACONST_NULL)

        false -> arrayOf(ICONST_0)
        true -> arrayOf(ICONST_1)

        -1 -> arrayOf(ICONST_M1)
        0 -> arrayOf(ICONST_0)
        1 -> arrayOf(ICONST_1)
        2 -> arrayOf(ICONST_2)
        3 -> arrayOf(ICONST_3)
        4 -> arrayOf(ICONST_4)
        5 -> arrayOf(ICONST_5)

        -1L -> arrayOf(ICONST_M1, I2L)
        0L -> arrayOf(LCONST_0)
        1L -> arrayOf(LCONST_1)
        2L -> arrayOf(ICONST_2, I2L)
        3L -> arrayOf(ICONST_3, I2L)
        4L -> arrayOf(ICONST_4, I2L)
        5L -> arrayOf(ICONST_5, I2L)

        -1F -> arrayOf(ICONST_M1, I2F)
        0F -> arrayOf(FCONST_0)
        1F -> arrayOf(FCONST_1)
        2F -> arrayOf(FCONST_2)
        3F -> arrayOf(ICONST_3, I2F)
        4F -> arrayOf(ICONST_4, I2F)
        5F -> arrayOf(ICONST_5, I2F)

        -1.0 -> arrayOf(ICONST_M1, I2D)
        0.0 -> arrayOf(DCONST_0)
        1.0 -> arrayOf(DCONST_1)
        2.0 -> arrayOf(ICONST_2, I2D)
        3.0 -> arrayOf(ICONST_3, I2D)
        4.0 -> arrayOf(ICONST_4, I2D)
        5.0 -> arrayOf(ICONST_5, I2D)

        isByte(value) -> arrayOf(BiPush((value as Number).toByte()))
        isShort(value) -> arrayOf(SiPush((value as Number).toShort()))
        else -> arrayOf(Ldc(value))
    }
    instr.forEach(this::instruct)
}*/

fun VMFunction.PUSH(value: Any?) = instruct(BasicInstruction { it.push(value) })

/*private fun isByte(value: Any?): Boolean {
    if (value is Short && value in -128..127) return true
    if (value is Int && value in -128..127) return true
    return value is Byte
}

private fun isShort(value: Any?): Boolean {
    if (value is Int && value in Short.MIN_VALUE..Short.MAX_VALUE) return true
    return value is Short
}
private class BiPush(private val value: Byte) : Instruction {
    override fun getByteCode() = byteArrayOf(0x10, value)
    override fun execute(method: VMMethod, context: ExecutionContext) {
        context.push(value.toInt())
        context.currentPointer++
    }
}
private class SiPush(private val value: Short) : Instruction {
    override fun getByteCode() = byteArrayOf(0x11, *value.bytes)
    override fun execute(method: VMMethod, context: ExecutionContext) {
        context.push(value.toInt())
        context.currentPointer++
    }
}
private class Ldc(private val value: Any?) : Instruction {
    override fun getByteCode() = byteArrayOf(0x12, 0x00, 0x00)
    override fun execute(method: VMMethod, context: ExecutionContext) {
        context.push(value)
        context.currentPointer++
    }
}

val Short.bytes get() = byteArrayOf(
        ((toInt() shr 8) and 0xFF).toByte(),
        (toInt() and 0xFF).toByte()
)
val Int.bytes get() = byteArrayOf(
        (toInt() shr 24).toByte(),
        (toInt() shr 16).toByte(),
        (toInt() shr 8).toByte(),
        toInt().toByte()
)
val Long.bytes get() = byteArrayOf(
        (this shr 56).toByte(),
        (this shr 48).toByte(),
        (this shr 40).toByte(),
        (this shr 32).toByte(),
        (this shr 24).toByte(),
        (this shr 16).toByte(),
        (this shr 8).toByte(),
        toByte()
)

private val ACONST_NULL = BasicInstruction(0x01) { it.push(null) }
private val ICONST_M1 = BasicInstruction(0x02) { it.push(-1) }
private val ICONST_0 = BasicInstruction(0x03) { it.push(0) }
private val ICONST_1 = BasicInstruction(0x04) { it.push(1) }
private val ICONST_2 = BasicInstruction(0x05) { it.push(2) }
private val ICONST_3 = BasicInstruction(0x06) { it.push(3) }
private val ICONST_4 = BasicInstruction(0x07) { it.push(4) }
private val ICONST_5 = BasicInstruction(0x08) { it.push(5) }

private val LCONST_0 = BasicInstruction(0x09) { it.push(0L) }
private val LCONST_1 = BasicInstruction(0x0A) { it.push(1L) }

private val FCONST_0 = BasicInstruction(0x0B) { it.push(0F) }
private val FCONST_1 = BasicInstruction(0x0C) { it.push(1F) }
private val FCONST_2 = BasicInstruction(0x0D) { it.push(2F) }

private val DCONST_0 = BasicInstruction(0x0E) { it.push(0.0) }
private val DCONST_1 = BasicInstruction(0x0F) { it.push(1.0) }*/

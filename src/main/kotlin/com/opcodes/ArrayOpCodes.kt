package com.opcodes

import com.vm.ExecutionContext
import com.vm.VMFunction
import com.vm.instruction.BasicInstruction
import com.vm.instruction.Instructions
import java.lang.ClassCastException
import java.lang.IllegalArgumentException

fun registerArrayInstructions() {
    Instructions.register("aload") { ALOAD() }
    Instructions.register("astore") { ASTORE() }

    Instructions.register("newarray") { NEWARRAY(it[1].toInt()) }
    Instructions.register("arraylength") { ARRAYLENGTH() }
    Instructions.register("multianewarray") { MULTIANEWARRAY(it[1].toInt()) }
}

fun VMFunction.ALOAD() = instruct(ALOAD)
fun VMFunction.ASTORE() = instruct(ASTORE)

fun VMFunction.NEWARRAY(type: Int) = instruct(BasicInstruction { it.push(getArrayType(type).create(it.pop() as Int)) })
fun VMFunction.ARRAYLENGTH() = instruct(ARRAYLENGTH)
fun VMFunction.MULTIANEWARRAY(dimensions: Int) = instruct(BasicInstruction { it.push(makeMultiArray(it, dimensions)) })

private fun getArrayType(type: Int): ArrayType {
    return when (type) {
        4 -> ArrayType.T_BOOLEAN
        5 -> ArrayType.T_CHAR
        6 -> ArrayType.T_FLOAT
        7 -> ArrayType.T_DOUBLE
        8 -> ArrayType.T_BYTE
        9 -> ArrayType.T_SHORT
        10 -> ArrayType.T_INT
        11 -> ArrayType.T_LONG
        12 -> ArrayType.T_OBJECT
        else -> throw IllegalArgumentException("$type out of range; must be in [4, 12]")
    }
}

private fun makeMultiArray(context: ExecutionContext, dimensions: Int): Array<Any?> {
    if (dimensions < 2) throw IllegalArgumentException("only use this instruction for multidimensional arrays")

    val firstSize = context.pop() as Int
    val factory = getFactory(context, dimensions) { arrayOfNulls<Any?>(firstSize) }
    return factory()
}

private val ALOAD = BasicInstruction() {
    val index = it.pop() as Int
    val value = when (val array = it.pop()) {
        is IntArray -> array[index]
        is LongArray -> array[index]
        is FloatArray -> array[index]
        is DoubleArray -> array[index]
        is Array<*> -> array[index]
        is ByteArray -> array[index]
        is CharArray -> array[index]
        is ShortArray -> array[index]
        else -> throw ClassCastException("$array is not a valid array type")
    }
    it.push(value)
}

private val ASTORE = BasicInstruction() {
    val value = it.pop()
    val index = it.pop() as Int
    when (val array = it.pop()) {
        is IntArray -> array[index] = value as Int
        is LongArray -> array[index] = value as Long
        is FloatArray -> array[index] = value as Float
        is DoubleArray -> array[index] = value as Double
        is Array<*> -> (array as Array<Any?>)[index] = value
        is ByteArray -> array[index] = value as Byte
        is CharArray -> array[index] = value as Char
        is ShortArray -> array[index] = value as Short
        else -> throw ClassCastException("$array is not a valid array type")
    }
}

private val ARRAYLENGTH = BasicInstruction() {
    val length = when (val array = it.pop()) {
        is BooleanArray -> array.size
        is CharArray -> array.size
        is FloatArray -> array.size
        is DoubleArray -> array.size
        is ByteArray -> array.size
        is ShortArray -> array.size
        is IntArray -> array.size
        is LongArray -> array.size
        is Array<*> -> array.size
        else -> throw ClassCastException("$array is not a valid array type")
    }
    it.push(length)
}

private fun getFactory(context: ExecutionContext, rem: Int, factory: () -> Array<Any?>): () -> Array<Any?> {
    if (rem == 1) return factory
    val size = context.pop() as Int
    return getFactory(context, rem - 1) { Array(size) { factory() } }
}

/*fun VMMethod.IALOAD() = instruct(IALOAD)
fun VMMethod.LALOAD() = instruct(LALOAD)
fun VMMethod.FALOAD() = instruct(FALOAD)
fun VMMethod.DALOAD() = instruct(DALOAD)
fun VMMethod.AALOAD() = instruct(AALOAD)
fun VMMethod.BALOAD() = instruct(BALOAD)
fun VMMethod.CALOAD() = instruct(CALOAD)
fun VMMethod.SALOAD() = instruct(SALOAD)*/
/*fun VMMethod.IASTORE() = instruct(IASTORE)
fun VMMethod.LASTORE() = instruct(LASTORE)
fun VMMethod.FASTORE() = instruct(FASTORE)
fun VMMethod.DASTORE() = instruct(DASTORE)
fun VMMethod.AASTORE() = instruct(AASTORE)
fun VMMethod.BASTORE() = instruct(BASTORE)
fun VMMethod.CASTORE() = instruct(CASTORE)
fun VMMethod.SASTORE() = instruct(SASTORE)*/
/*private fun <A, T> aload(get: A.(Int) -> T) = BasicInstruction {
    val index = it.pop() as Int
    val array = it.pop() as A
    it.push(array.get(index))
}
private val IALOAD = aload(IntArray::get)
private val LALOAD = aload(LongArray::get)
private val FALOAD = aload(FloatArray::get)
private val DALOAD = aload(DoubleArray::get)
private val AALOAD = aload(Array<Any?>::get)
private val BALOAD = aload(ByteArray::get)
private val CALOAD = aload(CharArray::get)
private val SALOAD = aload(ShortArray::get)*/
/*private fun <A, T> astore(set: A.(Int, T) -> Unit) = BasicInstruction {
    val value = it.pop() as T
    val index = it.pop() as Int
    val array = it.pop() as A
    array.set(index, value)
}
private val IASTORE = astore(IntArray::set)
private val LASTORE = astore(LongArray::set)
private val FASTORE = astore(FloatArray::set)
private val DASTORE = astore(DoubleArray::set)
private val AASTORE = astore(Array<Any?>::set)
private val BASTORE = astore(ByteArray::set)
private val CASTORE = astore(CharArray::set)
private val SASTORE = astore(ShortArray::set)*/

package com.opcodes

import com.vm.VMFunction
import com.vm.instruction.BasicInstruction
import com.vm.instruction.Instructions

fun registerMathInstructions() {
    Instructions.register("iadd", "i+") { IADD() }
    Instructions.register("ladd", "l+") { LADD() }
    Instructions.register("fadd", "f+") { FADD() }
    Instructions.register("dadd", "d+") { DADD() }

    Instructions.register("isub", "i-") { ISUB() }
    Instructions.register("lsub", "l-") { LSUB() }
    Instructions.register("fsub", "f-") { FSUB() }
    Instructions.register("dsub", "d-") { DSUB() }

    Instructions.register("imul", "i*") { IMUL() }
    Instructions.register("lmul", "l*") { LMUL() }
    Instructions.register("fmul", "f*") { FMUL() }
    Instructions.register("dmul", "d*") { DMUL() }

    Instructions.register("idiv", "i/") { IDIV() }
    Instructions.register("ldiv", "l/") { LDIV() }
    Instructions.register("fdiv", "f/") { FDIV() }
    Instructions.register("ddiv", "d/") { DDIV() }

    Instructions.register("irem", "i%") { IREM() }
    Instructions.register("lrem", "l%") { LREM() }
    Instructions.register("frem", "f%") { FREM() }
    Instructions.register("drem", "d%") { DREM() }

    Instructions.register("ineg", "-i") { INEG() }
    Instructions.register("lneg", "-l") { LNEG() }
    Instructions.register("fneg", "-f") { FNEG() }
    Instructions.register("dneg", "-d") { DNEG() }

    Instructions.register("ishl") { ISHL() }
    Instructions.register("lshl") { LSHL() }
    Instructions.register("ishr") { ISHR() }
    Instructions.register("lshr") { LSHR() }
    Instructions.register("iushr") { IUSHR() }
    Instructions.register("lushr") { LUSHR() }
    Instructions.register("iand") { IAND() }
    Instructions.register("land") { LAND() }
    Instructions.register("ior") { IOR() }
    Instructions.register("lor") { LOR() }
    Instructions.register("ixor") { IXOR() }
    Instructions.register("lxor") { LXOR() }

    Instructions.register("iinc") { IINC(it[1], it[2].toInt()) }
}

fun VMFunction.IADD() = instruct(IADD)
fun VMFunction.LADD() = instruct(LADD)
fun VMFunction.FADD() = instruct(FADD)
fun VMFunction.DADD() = instruct(DADD)

fun VMFunction.ISUB() = instruct(ISUB)
fun VMFunction.LSUB() = instruct(LSUB)
fun VMFunction.FSUB() = instruct(FSUB)
fun VMFunction.DSUB() = instruct(DSUB)

fun VMFunction.IMUL() = instruct(IMUL)
fun VMFunction.LMUL() = instruct(LMUL)
fun VMFunction.FMUL() = instruct(FMUL)
fun VMFunction.DMUL() = instruct(DMUL)

fun VMFunction.IDIV() = instruct(IDIV)
fun VMFunction.LDIV() = instruct(LDIV)
fun VMFunction.FDIV() = instruct(FDIV)
fun VMFunction.DDIV() = instruct(DDIV)

fun VMFunction.IREM() = instruct(IREM)
fun VMFunction.LREM() = instruct(LREM)
fun VMFunction.FREM() = instruct(FREM)
fun VMFunction.DREM() = instruct(DREM)

fun VMFunction.INEG() = instruct(INEG)
fun VMFunction.LNEG() = instruct(LNEG)
fun VMFunction.FNEG() = instruct(FNEG)
fun VMFunction.DNEG() = instruct(DNEG)

fun VMFunction.ISHL() = instruct(ISHL)
fun VMFunction.LSHL() = instruct(LSHL)
fun VMFunction.ISHR() = instruct(ISHR)
fun VMFunction.LSHR() = instruct(LSHR)
fun VMFunction.IUSHR() = instruct(IUSHR)
fun VMFunction.LUSHR() = instruct(LUSHR)
fun VMFunction.IAND() = instruct(IAND)
fun VMFunction.LAND() = instruct(LAND)
fun VMFunction.IOR() = instruct(IOR)
fun VMFunction.LOR() = instruct(LOR)
fun VMFunction.IXOR() = instruct(IXOR)
fun VMFunction.LXOR() = instruct(LXOR)

fun VMFunction.IINC(index: String, amount: Int) = instruct(BasicInstruction() { it.store(index, (it.load(index) as Int) + amount) })

private fun <T> op(op: T.(T) -> T) = BasicInstruction() {
    val rhs = it.pop() as T
    it.push((it.pop() as T).op(rhs))
}
private fun <T> neg(op: T.() -> T) = BasicInstruction() { it.push((it.pop() as T).op()) }
private fun <T> bit(op: T.(Int) -> T) = BasicInstruction() {
    val rhs = it.pop() as Int
    it.push((it.pop() as T).op(rhs))
}


private val IADD = op<Int>(Int::plus)
private val LADD = op<Long>(Long::plus)
private val FADD = op<Float>(Float::plus)
private val DADD = op<Double>(Double::plus)

private val ISUB = op<Int>(Int::minus)
private val LSUB = op<Long>(Long::minus)
private val FSUB = op<Float>(Float::minus)
private val DSUB = op<Double>(Double::minus)

private val IMUL = op<Int>(Int::times)
private val LMUL = op<Long>(Long::times)
private val FMUL = op<Float>(Float::times)
private val DMUL = op<Double>(Double::times)

private val IDIV = op<Int>(Int::div)
private val LDIV = op<Long>(Long::div)
private val FDIV = op<Float>(Float::div)
private val DDIV = op<Double>(Double::div)

private val IREM = op<Int>(Int::rem)
private val LREM = op<Long>(Long::rem)
private val FREM = op<Float>(Float::rem)
private val DREM = op<Double>(Double::rem)

private val INEG = neg(Int::unaryMinus)
private val LNEG = neg(Long::unaryMinus)
private val FNEG = neg(Float::unaryMinus)
private val DNEG = neg(Double::unaryMinus)

private val ISHL = bit(Int::shl)
private val LSHL = bit(Long::shl)
private val ISHR = bit(Int::shr)
private val LSHR = bit(Long::shr)
private val IUSHR = bit(Int::ushr)
private val LUSHR = bit(Long::ushr)
private val IAND = op(Int::and)
private val LAND = op(Long::and)
private val IOR = op(Int::or)
private val LOR = op(Long::or)
private val IXOR = op(Int::xor)
private val LXOR = op(Long::xor)

package com.opcodes

import com.vm.VMFunction
import com.vm.instruction.BasicInstruction
import com.vm.instruction.Instructions

fun registerStackInstructions() {
    Instructions.register("nop") { NOP() }
    Instructions.register("pop") { POP() }
    Instructions.register("dup") { DUP() }
    Instructions.register("dupx") { DUPX(it[1].toInt()) }
    Instructions.register("swap") { SWAP() }
}

fun VMFunction.NOP() = instruct(NOP)
fun VMFunction.POP() = instruct(POP)
fun VMFunction.DUP() = instruct(DUP)
fun VMFunction.SWAP() = instruct(SWAP)

fun VMFunction.DUPX(amount: Int) {
    val instruction = BasicInstruction() {
        val otherStack = Array(amount) { _ -> it.pop() }
        it.push(otherStack[0])
        otherStack.also { i -> i.reverse() }.forEach { i -> it.push(i) }
    }
    instruct(instruction)
}

private val NOP = BasicInstruction() {}
private val POP = BasicInstruction() { it.pop() }
private val DUP = BasicInstruction() { it.push(it.peek()) }

private val SWAP = BasicInstruction() {
    val first = it.pop()
    val second = it.pop()
    it.push(first)
    it.push(second)
}

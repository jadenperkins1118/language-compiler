package com.opcodes

import com.vm.ExecutionContext
import com.vm.Label
import com.vm.VMFunction
import com.vm.instruction.BasicInstruction
import com.vm.instruction.GotoInstruction
import com.vm.instruction.Instructions
import com.vm.instruction.JumpInstruction


fun registerCompareInstructions() {
    Instructions.register("lcmp") { LCMP() }
    Instructions.register("fcmpl") { FCMPL() }
    Instructions.register("fcmpg") { FCMPG() }
    Instructions.register("dcmpl") { DCMPL() }
    Instructions.register("dcmpg") { DCMPG() }

//    Instructions.register("ifeq", "==") { IFEQ(it[1]) }
//    Instructions.register("ifne", "!=") { IFNE(it[1]) }
//    Instructions.register("iflt", "<") { IFLT(it[1]) }
//    Instructions.register("ifge", ">=") { IFGE(it[1]) }
//    Instructions.register("ifgt", ">") { IFGT(it[1]) }
//    Instructions.register("ifle", "<=") { IFLE(it[1]) }
//
//    Instructions.register("if_icmpeq", "i==") { IF_ICMPEQ(it[1]) }
//    Instructions.register("if_icmpne", "i!=") { IF_ICMPNE(it[1]) }
//    Instructions.register("if_icmplt", "i<") { IF_ICMPLT(it[1]) }
//    Instructions.register("if_icmpge", "i>=") { IF_ICMPGE(it[1]) }
//    Instructions.register("if_icmpgt", "i>") { IF_ICMPGT(it[1]) }
//    Instructions.register("if_icmple", "i<=") { IF_ICMPLE(it[1]) }
//
//    Instructions.register("if_acmpeq", "a==") { IF_ACMPEQ(it[1]) }
//    Instructions.register("if_acmpne", "a!=") { IF_ACMPNE(it[1]) }
//
//    Instructions.register("goto") { GOTO(it[1]) }
//
//    Instructions.register("ifnull", "null") { IFNULL(it[1]) }
//    Instructions.register("ifnonnull", "!null") { IFNONNULL(it[1]) }
}

fun VMFunction.LCMP() = instruct(LCMP)
fun VMFunction.FCMPL() = instruct(FCMPL)
fun VMFunction.FCMPG() = instruct(FCMPG)
fun VMFunction.DCMPL() = instruct(DCMPL)
fun VMFunction.DCMPG() = instruct(DCMPG)

private val LCMP = BasicInstruction {
    val rhs = it.pop() as Long
    it.push((it.pop() as Long).compareTo(rhs))
}

private fun <T> compDec(def: Int, isNaN: T.() -> Boolean, compareTo: T.(T) -> Int) = BasicInstruction {
    val rhs = it.pop() as T
    val lhs = it.pop() as T
    it.push(if (lhs.isNaN() || rhs.isNaN()) def else lhs.compareTo(rhs))
}
private val FCMPL = compDec(-1, Float::isNaN, Float::compareTo)
private val FCMPG = compDec(1, Float::isNaN, Float::compareTo)
private val DCMPL = compDec(-1, Double::isNaN, Double::compareTo)
private val DCMPG = compDec(1, Double::isNaN, Double::compareTo)

private fun VMFunction.jump(label: Label, compare: (Int, Int) -> Boolean, get: (ExecutionContext) -> Int) {
    val instruction = JumpInstruction(label) {
        val rhs = get(it)
        compare(it.pop() as Int, rhs)
    }
    instruct(instruction)
}

private fun VMFunction.jump0(label: Label, logic: (Int, Int) -> Boolean) = jump(label, logic) { 0 }
fun VMFunction.IFEQ(label: Label) = jump0(label) { a, b -> a == b }
fun VMFunction.IFNE(label: Label) = jump0(label) { a, b -> a != b }
fun VMFunction.IFLT(label: Label) = jump0(label) { a, b -> a < b }
fun VMFunction.IFGE(label: Label) = jump0(label) { a, b -> a >= b }
fun VMFunction.IFGT(label: Label) = jump0(label) { a, b -> a > b }
fun VMFunction.IFLE(label: Label) = jump0(label) { a, b -> a < b }

private fun VMFunction.jumpPop(label: Label, logic: (Int, Int) -> Boolean) = jump(label, logic) { it.pop() as Int }
fun VMFunction.IF_ICMPEQ(label: Label) = jumpPop(label) { a, b -> a == b }
fun VMFunction.IF_ICMPNE(label: Label) = jumpPop(label) { a, b -> a != b }
fun VMFunction.IF_ICMPLT(label: Label) = jumpPop(label) { a, b -> a < b }
fun VMFunction.IF_ICMPGE(label: Label) = jumpPop(label) { a, b -> a >= b }
fun VMFunction.IF_ICMPGT(label: Label) = jumpPop(label) { a, b -> a > b }
fun VMFunction.IF_ICMPLE(label: Label) = jumpPop(label) { a, b -> a <= b }

fun VMFunction.IF_ACMPEQ(label: Label) = instruct(JumpInstruction(label) {
    val rhs = it.pop()
    it.pop() == rhs
})
fun VMFunction.IF_ACMPNE(label: Label) = instruct(JumpInstruction(label) {
    val rhs = it.pop()
    it.pop() != rhs
})

fun VMFunction.GOTO(label: Label) = instruct(GotoInstruction(label))

fun VMFunction.IFNULL(label: Label) = instruct(JumpInstruction(label) { it.pop() == null })
fun VMFunction.IFNONNULL(label: Label) = instruct(JumpInstruction(label) { it.pop() != null })

private val TABLESWITCH = 0xAA //16+: [0–3 bytes padding], defaultbyte1, defaultbyte2, defaultbyte3, defaultbyte4, lowbyte1, lowbyte2, lowbyte3, lowbyte4, highbyte1, highbyte2, highbyte3, highbyte4, jump offsets...	index →	continue execution from an address in the table at offset index
private val LOOKUPSWITCH = 0xAB //8+: <0–3 bytes padding>, defaultbyte1, defaultbyte2, defaultbyte3, defaultbyte4, npairs1, npairs2, npairs3, npairs4, match-offset pairs...	key →	a target address is looked up from a table using a key and execution continues from the instruction at that address

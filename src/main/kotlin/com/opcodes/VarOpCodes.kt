package com.opcodes

import com.vm.VMFunction
import com.vm.instruction.BasicInstruction
import com.vm.instruction.Instructions

fun registerVarInstructions() {
    Instructions.register("load") { LOAD(it[1]) }
    Instructions.register("store") { STORE(it[1]) }
}

fun VMFunction.LOAD(index: String) = instruct(BasicInstruction() { it.push(it.load(index)) })
fun VMFunction.STORE(index: String) = instruct(BasicInstruction() { it.store(index, it.pop()) })

package com.opcodes

import com.vm.ExecutionContext
import com.vm.ScriptCallable
import com.vm.VMFunction
import com.vm.instruction.BasicInstruction
import com.vm.instruction.Instructions
import com.vm.symbol.ChildSymbols
import java.lang.RuntimeException
import kotlin.reflect.KFunction
import kotlin.reflect.jvm.isAccessible

fun registerCallInstructions() {
    Instructions.register("call") { CALL(it[1]) }
    Instructions.register("calldynamic") { CALLDYNAMIC() }
    Instructions.register("throw") { THROW(it[1]) }
}

fun VMFunction.INVOKE() = instruct(BasicInstruction { context ->
    val args = ArrayList<Any?>()
    while (context.peek() !is ScriptCallable) {
        args.add(context.pop())
    }

    val argArray = args.reversed().toTypedArray()

    val method = context.pop() as ScriptCallable
    val ret = method.invoke(context, argArray)
    if (ret !== Unit) context.push(ret)
})

fun VMFunction.CALL(name: String) = instruct(BasicInstruction { it.callMethod(name) })
fun VMFunction.CALLDYNAMIC() = instruct(CALLDYNAMIC)

fun VMFunction.THROW(message: String) = instruct(BasicInstruction { throw RuntimeException(message) })

private val CALLDYNAMIC = BasicInstruction { it.callMethod(it.pop() as String) }

private fun ExecutionContext.callMethod(name: String) {
    val method = vm.getFunction(name)
    val args = if (method.parameters.isEmpty()) emptyMap() else {
        val argArray = Array(method.parameters.size) { pop() }.also { it.reverse() }
        method.parameters.withIndex().associate { (index, parameter) -> parameter to argArray[index] }
    }
    val ret = method.execute(vm, ChildSymbols.from(symbols, args))
    if (ret !== Unit) push(ret)
}

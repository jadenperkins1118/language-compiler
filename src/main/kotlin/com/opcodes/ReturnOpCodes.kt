package com.opcodes

import com.vm.VMFunction
import com.vm.instruction.Instructions
import com.vm.instruction.ReturnInstruction

fun registerReturnInstructions() {
    Instructions.register("ireturn") { IRETURN() }
    Instructions.register("lreturn") { LRETURN() }
    Instructions.register("freturn") { FRETURN() }
    Instructions.register("dreturn") { DRETURN() }
    Instructions.register("areturn") { ARETURN() }
    Instructions.register("return") { RETURN() }
}

fun VMFunction.IRETURN() = instruct(IRETURN)
fun VMFunction.LRETURN() = instruct(LRETURN)
fun VMFunction.FRETURN() = instruct(FRETURN)
fun VMFunction.DRETURN() = instruct(DRETURN)
fun VMFunction.ARETURN() = instruct(ARETURN)
fun VMFunction.RETURN() = instruct(RETURN)

private val IRETURN = ReturnInstruction() { it.pop() as Int }
private val LRETURN = ReturnInstruction() { it.pop() as Long }
private val FRETURN = ReturnInstruction() { it.pop() as Float }
private val DRETURN = ReturnInstruction() { it.pop() as Double }
private val ARETURN = ReturnInstruction() { it.pop() }
private val RETURN = ReturnInstruction() { Unit }

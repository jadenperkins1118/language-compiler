package com.opcodes

import com.vm.VMFunction
import com.vm.instruction.BasicInstruction
import com.vm.instruction.Instructions

fun registerConvertInstructions() {
    Instructions.register("i2l") { I2L() }
    Instructions.register("i2f") { I2F() }
    Instructions.register("i2d") { I2D() }

    Instructions.register("l2i") { L2I() }
    Instructions.register("l2f") { L2F() }
    Instructions.register("l2d") { L2D() }

    Instructions.register("f2i") { F2I() }
    Instructions.register("f2l") { F2L() }
    Instructions.register("f2d") { F2D() }

    Instructions.register("d2i") { D2I() }
    Instructions.register("d2l") { D2L() }
    Instructions.register("d2f") { D2F() }
}

fun VMFunction.I2L() = instruct(I2L)
fun VMFunction.I2F() = instruct(I2F)
fun VMFunction.I2D() = instruct(I2D)

fun VMFunction.L2I() = instruct(L2I)
fun VMFunction.L2F() = instruct(L2F)
fun VMFunction.L2D() = instruct(L2D)

fun VMFunction.F2I() = instruct(F2I)
fun VMFunction.F2L() = instruct(F2L)
fun VMFunction.F2D() = instruct(F2D)

fun VMFunction.D2I() = instruct(D2I)
fun VMFunction.D2L() = instruct(D2L)
fun VMFunction.D2F() = instruct(D2F)


private fun <A, B> convert(op: (A) -> B) = BasicInstruction { it.push(op(it.pop() as A)) }

private val I2L = convert(Int::toLong)
private val I2F = convert(Int::toFloat)
private val I2D = convert(Int::toDouble)

private val L2I = convert(Long::toInt)
private val L2F = convert(Long::toFloat)
private val L2D = convert(Long::toDouble)

private val F2I = convert(Float::toInt)
private val F2L = convert(Float::toLong)
private val F2D = convert(Float::toDouble)

private val D2I = convert(Double::toInt)
private val D2L = convert(Double::toLong)
private val D2F = convert(Double::toFloat)

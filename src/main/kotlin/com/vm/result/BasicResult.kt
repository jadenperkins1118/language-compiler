package com.vm.result

object BasicResult : Result {
    override val delta = 1
    override val value: Any? = null

    override fun toString(): String {
        return "no result"
    }
}
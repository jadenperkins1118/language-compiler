package com.vm.result

class ReturnResult(override val value: Any?) : Result {
    override val delta = 0

    override fun toString(): String {
        return "Returning ${value.toString()}"
    }
}
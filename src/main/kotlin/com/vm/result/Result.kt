package com.vm.result

interface Result {
    val delta: Int
    val value: Any?
}
package com.vm.result

class JumpResult(override val delta: Int) : Result {
    override val value: Any? = null

    override fun toString(): String {
        return "jumping $delta"
    }
}
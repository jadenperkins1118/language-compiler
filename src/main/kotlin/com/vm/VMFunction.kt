package com.vm

import com.vm.instruction.Instruction
import com.vm.symbol.ChildSymbols
import com.vm.symbol.GlobalSymbols
import com.vm.symbol.SymbolTable
import java.lang.IllegalStateException

class VMFunction(val name: String, val parameters: Array<String>, setup: VMFunction.() -> Unit) : ScriptCallable {
    private val instructions = ArrayList<Instruction>()
    private val labels = HashMap<Label, Int>()

    init {
        setup()
    }

    fun getInstruction(pointer: Int) = instructions[pointer]

    fun instruct(instruction: Instruction) {
        instructions.add(instruction)
    }

    /**Returns the current last instruction in the list*/
    fun peek() = instructions.last()

    fun label(label: Label) {
        labels[label] = instructions.size
    }

    fun getPC(label: Label) = labels[label] ?: throw NoSuchElementException("$label is not a registered label")

    fun execute(vm: VM, args: SymbolTable = GlobalSymbols.from(emptyMap())): Any? {
        require(args.size == parameters.size) { "method called with ${args.size}, expected ${parameters.size}" }

        val context = ExecutionContext(vm, args)
        var pc = 0
        while (pc in instructions.indices) {
            val result = vm.strategy.execute(this, context, pc, getInstruction(pc))
            if (result.delta == 0) return result.value
            pc += result.delta
        }
        throw IllegalStateException("pc $pc is outside of instruction range; function may not have returned")
    }

    override fun invoke(context: ExecutionContext, args: Array<Any?>): Any? {
        val argMap = parameters.withIndex().associate { (index, parameter) -> parameter to args[index] }
        return execute(context.vm, ChildSymbols.from(context.symbols, argMap))
    }
}
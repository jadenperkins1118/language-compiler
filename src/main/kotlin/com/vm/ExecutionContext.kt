package com.vm

import com.vm.symbol.SymbolTable
import java.util.ArrayDeque

class ExecutionContext(val vm: VM, val symbols: SymbolTable) {
    private val operandStack = ArrayDeque<Ref>()

    fun push(value: Any?) {
        val real = when (value) {
            true -> Ref(1)
            false -> Ref(0)
            is String, is Number, is ScriptObject, is ScriptCallable, null -> Ref(value)
            else -> Ref(ScriptObjectWrapper(value))
        }
        operandStack.push(real)
    }
    fun pop(): Any? = operandStack.pop().value
    fun peek(): Any? = operandStack.peek().value

    fun store(name: String, value: Any?) = symbols.store(name, value)
    fun load(name: String) = symbols.load(name)

    fun getStackString() = operandStack.toString()
    fun getVariableString() = symbols.toString()
}

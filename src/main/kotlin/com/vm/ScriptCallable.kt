package com.vm

interface ScriptCallable {
    fun invoke(context: ExecutionContext, args: Array<Any?>): Any?
}
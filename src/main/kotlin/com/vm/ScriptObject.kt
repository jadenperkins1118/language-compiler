package com.vm

interface ScriptObject {
    fun getMember(context: ExecutionContext, name: String): Any?
}
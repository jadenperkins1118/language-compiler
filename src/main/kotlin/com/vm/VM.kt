package com.vm

import com.vm.strategy.ExecutionStrategy
import com.vm.symbol.GlobalSymbols
import com.vm.symbol.SymbolTable

class VM(private val methods: Map<String, VMFunction>, val strategy: ExecutionStrategy) {
    fun getFunction(name: String) = methods[name] ?: throw NoSuchElementException("$name method does not exist")
    fun execute(symbols: SymbolTable = GlobalSymbols.from(emptyMap())) = getFunction("main").execute(this, symbols)
}
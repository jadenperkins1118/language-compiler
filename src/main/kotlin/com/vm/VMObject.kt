package com.vm

import com.asString

class VMObject : ScriptObject {
    private val fields = HashMap<String, Any?>()

    operator fun set(name: String, value: Any?) {
        fields[name] = value
    }

    operator fun get(name: String) = fields[name]
    operator fun contains(name: String) = name in fields
    fun delField(name: String) = fields.remove(name)

    override fun toString(): String {
        return "{" + fields.map { it.key + "=" + it.value.asString() }.joinToString(", ") + "}"
    }

    override fun getMember(context: ExecutionContext, name: String) {
        context.push(get(name))
    }
}
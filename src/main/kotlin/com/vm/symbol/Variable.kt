package com.vm.symbol

import java.lang.UnsupportedOperationException

interface Variable {
    fun get(): Any?
    fun set(value: Any?) {
        throw UnsupportedOperationException("variable cannot have its value set")
    }
}
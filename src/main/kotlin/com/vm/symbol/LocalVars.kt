package com.vm.symbol

import java.lang.IllegalArgumentException

class LocalVars {
    private val indices = HashMap<String, Int>()
    private val values = HashMap<Int, Any?>()

    fun store(name: String, value: Any?) {
        val index = indices.getOrPut(name) { indices.size }
        values[index] = value
    }

    fun load(name: String): Any? {
        val index = indices[name] ?: throw IllegalArgumentException("no variable named $name")
        if (index !in values) throw NoSuchElementException("index $index is not available for $name")
        return values[index]
    }

    override fun toString(): String {
        return indices.map { (name, index) -> "$index[$name]=${values[index]}" }.joinToString(", ")
    }
}
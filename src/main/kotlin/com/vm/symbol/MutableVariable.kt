package com.vm.symbol

class MutableVariable(private var value: Any?) : Variable {
    override fun get() = value

    override fun set(value: Any?) {
        this.value = value
    }
}
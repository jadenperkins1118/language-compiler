package com.vm.symbol

class GlobalSymbols : SymbolTable {
    private val values = HashMap<String, Any?>()

    override val size get() = values.size

    override fun store(name: String, value: Any?) {
        values[name] = value
    }

    override fun load(name: String) = values[name] ?: Unit

    override fun toString() = values.toString()

    companion object {
        fun from(map: Map<String, Any?>): SymbolTable {
            val ret = GlobalSymbols()
            map.forEach(ret::store)
            return ret
        }
    }
}
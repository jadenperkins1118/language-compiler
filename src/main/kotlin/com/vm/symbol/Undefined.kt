package com.vm.symbol

object Undefined : Variable {
    override fun get() = this
}
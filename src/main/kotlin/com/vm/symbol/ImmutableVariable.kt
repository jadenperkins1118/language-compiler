package com.vm.symbol

class ImmutableVariable(private val value: Any?) : Variable {
    override fun get() = value
}
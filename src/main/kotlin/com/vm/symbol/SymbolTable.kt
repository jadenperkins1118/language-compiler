package com.vm.symbol

interface SymbolTable {
    val size: Int
    fun store(name: String, value: Any?)
    fun load(name: String): Any?
}
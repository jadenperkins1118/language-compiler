package com.vm.symbol

class ChildSymbols(private val parent: SymbolTable) : SymbolTable {
    private val values = HashMap<String, Any?>()

    override val size get() = values.size

    override fun store(name: String, value: Any?) {
        val global = parent.load(name)
        if (global === Unit) {
            values[name] = value
        } else {
            parent.store(name, value)
        }
    }

    override fun load(name: String) = values[name] ?: parent.load(name)

    override fun toString() = values.toString()

    companion object {
        fun from(parent: SymbolTable, map: Map<String, Any?>): SymbolTable {
            val ret = ChildSymbols(parent)
            map.forEach { (key, value) -> ret.values[key] = value }
            return ret
        }
    }
}
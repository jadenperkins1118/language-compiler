package com.vm.instruction

import com.vm.ExecutionContext
import com.vm.Label
import com.vm.result.Result
import com.vm.VMFunction
import com.vm.result.JumpResult

class JumpInstruction(private val label: Label, private val compare: (ExecutionContext) -> Boolean) : Instruction {
    override fun execute(function: VMFunction, context: ExecutionContext, pc: Int): Result {
        val offset = if (compare(context)) function.getPC(label) - pc else 1
        return JumpResult(offset)
    }
}
package com.vm.instruction

import com.vm.ExecutionContext
import com.vm.result.Result
import com.vm.VMFunction
import com.vm.result.BasicResult

class BasicInstruction(private val operation: VMFunction.(ExecutionContext) -> Unit) : Instruction {
    override fun execute(function: VMFunction, context: ExecutionContext, pc: Int): Result {
        function.operation(context)
//        context.pc++
        return BasicResult
    }
}
package com.vm.instruction

import com.vm.ExecutionContext
import com.vm.VMFunction
import com.vm.result.ReturnResult

class ReturnInstruction(private val pop: (ExecutionContext) -> Any?) : Instruction {
    override fun execute(function: VMFunction, context: ExecutionContext, pc: Int) = ReturnResult(pop(context))
}
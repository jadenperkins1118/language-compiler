package com.vm.instruction

import com.vm.ExecutionContext
import com.vm.result.Result
import com.vm.VMFunction

interface Instruction {
    fun execute(function: VMFunction, context: ExecutionContext, pc: Int): Result
}
package com.vm.instruction

import com.opcodes.*
import com.vm.VMFunction
import java.lang.IllegalArgumentException

object Instructions {
    private val instructionFactories = HashMap<String, VMFunction.(Array<String>) -> Unit>()

    init {
        registerArrayInstructions()
        registerCallInstructions()
        registerCompareInstructions()
        registerConvertInstructions()
        registerMathInstructions()
        registerObjectInstructions()
        registerPushInstructions()
        registerReturnInstructions()
        registerStackInstructions()
        registerVarInstructions()

//        register("label") { label(it[1]) }
    }

    fun register(name: String, factory: VMFunction.(Array<String>) -> Unit) {
        instructionFactories[name.toLowerCase()] = factory
    }

    fun register(vararg names: String, factory: VMFunction.(Array<String>) -> Unit) {
        names.forEach { register(it, factory) }
    }

    fun factorize(function: VMFunction, tokens: Array<String>) {
        val name = tokens[0].toLowerCase()
        val factory = instructionFactories[name] ?: throw IllegalArgumentException("no factory found for $name; make sure $name is a valid instruction")
        function.factory(tokens)
    }
}
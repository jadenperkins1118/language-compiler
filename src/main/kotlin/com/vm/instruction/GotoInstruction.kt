package com.vm.instruction

import com.vm.ExecutionContext
import com.vm.Label
import com.vm.VMFunction
import com.vm.result.JumpResult

class GotoInstruction(private val label: Label) : Instruction {
    override fun execute(function: VMFunction, context: ExecutionContext, pc: Int) = JumpResult(function.getPC(label) - pc)
}
package com.vm.strategy

import com.vm.ExecutionContext
import com.vm.VMFunction
import com.vm.instruction.Instruction

object SimpleExecutionStrategy : ExecutionStrategy {
    override fun execute(function: VMFunction, context: ExecutionContext, pc: Int, instruction: Instruction) = function.getInstruction(pc).execute(function, context, pc)
}
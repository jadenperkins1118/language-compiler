package com.vm.strategy

import com.vm.ExecutionContext
import com.vm.result.Result
import com.vm.VMFunction
import com.vm.instruction.Instruction

interface ExecutionStrategy {
    fun execute(function: VMFunction, context: ExecutionContext, pc: Int, instruction: Instruction): Result
}
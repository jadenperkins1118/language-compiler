package com.vm.strategy

import com.time
import com.vm.ExecutionContext
import com.vm.result.Result
import com.vm.VMFunction
import com.vm.instruction.Instruction

class LoggingExecutionStrategy(
        private val showName: Boolean = true,
        private val showPointer: Boolean = true,
        private val showStack: Boolean = true,
        private val showVars: Boolean = true,
        private val showResult: Boolean = true,
        private val showTime: Boolean = true
) : ExecutionStrategy {
    override fun execute(function: VMFunction, context: ExecutionContext, pc: Int, instruction: Instruction): Result {
        val (result, time) = time { instruction.execute(function, context, pc) }

        val builder = StringBuilder()
        if (showName) builder.append(function.name).append("-")
        if (showPointer) builder.append(pc)
        if (showStack) {
            if (showPointer) builder.append(": ")
            builder.append(context.getStackString())
        }
        if (showVars) {
            if (showStack) builder.append(", ")
            else if (showPointer) builder.append(": ")
            builder.append(context.getVariableString())
        }

        if (showResult) builder.append(" -> ").append(result)
        if (showTime) builder.append("(").append(time).append(")")
        println(builder.toString())
        return result
    }
}
package com.vm

import java.lang.reflect.Method
import java.lang.reflect.Modifier

class ScriptObjectWrapper(private val source: Any) : ScriptObject {
    override fun getMember(context: ExecutionContext, name: String): Any? {
        source.javaClass.fields.find { it.name == name }?.let { return it.get(source) }
        val method = source.javaClass.methods.find { it.name == name } ?: throw NoSuchFieldException("no field or method named $name")

        return ScriptMethodWrapper(source, method)
    }
}

class ScriptMethodWrapper(private val source: Any, private val method: Method) : ScriptCallable {
    override fun invoke(context: ExecutionContext, args: Array<Any?>): Any? {
        if (method.modifiers and Modifier.PUBLIC != Modifier.PUBLIC) throw IllegalAccessException("cannot call non-public method")
        if (method.modifiers and Modifier.FINAL == Modifier.FINAL) method.isAccessible = true
        val ret = method.invoke(source, *args)

        if (method.returnType === Void.TYPE) return Unit
        return ret
    }
}
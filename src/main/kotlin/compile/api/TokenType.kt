package compile.api

import java.lang.UnsupportedOperationException

interface TokenType {
    val priority get() = 0
    fun addDelimiter(delimiters: MutableCollection<String>) {

    }
    fun matches(lexeme: String): Boolean
    fun getToken(lexeme: String, line: Int, literal: Any?) = Token(this, lexeme, line, literal)

    fun binaryExpression(left: Expression, right: Expression): Expression = error("binary")
    fun unaryExpression(value: Expression): Expression = error("unary")

    private fun error(type: String): Nothing {
        throw UnsupportedOperationException("this token doesn't produce a $type expression")
    }
}
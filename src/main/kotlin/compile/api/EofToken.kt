package compile.api

object EofToken : TokenType {
    override val priority: Int = 0
    override fun toString() = "EOF"
    override fun matches(lexeme: String) = false
    override fun getToken(lexeme: String, line: Int, literal: Any?) = Token(this, lexeme, line, literal)
}
package compile.api

data class Token(val type: TokenType, val lexeme: String, val line: Int, val literal: Any?)
package compile.api

import com.vm.VMFunction

interface Expression {
    /**All [Expression]s must output their instructions to the method; literals simply add a push instruction with
     * their literal value, more complex [Expression]s will call to nested [Expression]s and will result in
     * more complex instructions*/
    fun outputCode(method: VMFunction)

    /**Implemented by accessor [Expression]s, it loads additional instructions to the method before a compound
     * assignment operation occurs*/
    fun compoundPreAssign(method: VMFunction) {}
    /**Implemented by accessor [Expression]s, it loads instructions to the method before an assignment
     * operation occurs*/
    fun preAssign(method: VMFunction) {}
    /**Implemented by accessor and identifier [Expression]s, it loads instructions to the method after an
     * assignment operation occurs*/
    fun assign(method: VMFunction) {}
}
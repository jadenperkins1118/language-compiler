package compile.api

import java.lang.RuntimeException

abstract class Parser(private val tokens: List<Token>) {
    abstract fun parse(): Expression

    fun consume(token: TokenType, message: String): Token {
        if (check(token)) return advance()
        throw RuntimeException("$token: $message")
    }

    fun expect(type: TokenType): Token {
        val token = advance()
        check(token.type === type) { "expected $type" }
        return token
    }

    fun match(vararg types: TokenType): Boolean {
        for (type in types) {
            if (check(type)) {
                advance()
                return true
            }
        }
        return false
    }

    private fun check(type: TokenType): Boolean {
        return curr().type === type
    }

    private var pos = 0

    fun advance(): Token {
        val ret = curr()
        if (!isAtEnd()) pos++
        return ret
    }

    fun isAtEnd() = curr().type === EofToken

    fun prev() = get(-1)
    fun curr() = tokens[pos]
    fun next() = get(1)

    operator fun get(amt: Int) = tokens[pos + amt]
}
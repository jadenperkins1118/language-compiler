package me.jaden.parser

import compile.api.Expression
import compile.api.Parser
import compile.api.TokenType
import me.jaden.token.Operators

inline fun <R> Parser.expectWrap(left: TokenType, right: TokenType, block: Parser.() -> R): R {
    expect(left)
    val ret = block()
    expect(right)
    return ret
}

inline fun <R> Parser.expectParenthesis(block: Parser.() -> R) = expectWrap(Operators.LParen, Operators.RParen, block)

inline fun <T : Parser, R> doWhileMatch(parser: T, match: TokenType, expect: TokenType, block: (T) -> R): List<R> {
    if (parser.match(expect)) return emptyList()
    val ret = ArrayList<R>()
    do {
        ret.add(block(parser))
    } while (parser.match(match))
    parser.expect(expect)

    return ret
}

inline fun <T : Parser> decideExpression(parser: T, tokens: Array<TokenType>, parent: (T) -> Expression): Expression {
    var expression = parent(parser)

    while (parser.curr().type in tokens) {
        val token = parser.advance()
        val right = parent(parser)
        expression = token.type.binaryExpression(expression, right)
    }
    return expression
}

inline fun <R> attempt(block: () -> R) = try {
    block()
} catch (e: Throwable) {
    null
}
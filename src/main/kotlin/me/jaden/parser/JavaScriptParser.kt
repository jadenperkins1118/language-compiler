package me.jaden.parser

import compile.api.*
import me.jaden.expression.*
import java.lang.Exception

class JavaScriptParser(tokens: List<Token>) : Parser(tokens) {
    override fun parse(): Expression {
        try {
            val expressions = ArrayList<Expression>()
            while (!match(EofToken)) expressions.add(ExpressionHelper.statement(this))
            return ExpressionList(expressions)
        } catch (e: Exception) {
            System.err.println("\tprev = ${attempt { prev() }}")
            System.err.println("\tcurr = ${attempt { curr() }}")
            System.err.println("\tnext = ${attempt { next() }}")
            throw e
        }
    }
}
package me.jaden.token

import compile.api.EofToken
import compile.api.Token
import compile.api.TokenType
import org.reflections.Reflections
import java.lang.StringBuilder
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern
import kotlin.collections.ArrayList

object Tokens {
    private val keywordTokens = Keywords.tokens()

    private val tokenReaders = arrayOf(
            *Operators.tokens(),
            NumberToken,
            StringToken,
            IdentifierToken
    )

    private val pattern = getPattern()


    private fun getReaders(): List<TokenType> {
        val reflections = Reflections("me/jaden/token")
        val readers = reflections.getSubTypesOf(TokenType::class.java)

        val list = ArrayList<TokenType>()
        for (target in readers) {
            if (target.kotlin.isAbstract) continue
            val instance = target.kotlin.objectInstance ?: target.newInstance()
            if (instance.priority < 0) continue
            list += instance
        }

        return list.sortedBy { it.priority }
    }

    private fun getPattern(): Pattern {
        val delimiters = HashSet<String>()
        delimiters.add(" ")
        delimiters.add("\r")
        delimiters.add("\n")
        tokenReaders.forEach { it.addDelimiter(delimiters) }

        val sorted = delimiters.sortedByDescending { it }
        val result = buildString {
            append('(')
            for (delimiter in sorted) {
                if (length != 1) append("|")
                append("\\Q").append(delimiter).append("\\E")
            }
            append(')')
        }
        return Pattern.compile(result)
    }

    private val matchFunctions = listOf(
            ::tryIgnoreToken,
            ::trySingleLineComment,
            ::tryMultiLineComment,
            ::tryStringLiteral,
            ::trySmartToken
    )

    private fun tryMatch(ctx: TokenizerContext): Boolean {
        for (function in matchFunctions) {
            if (function(ctx)) return true
        }

        return false
    }

    fun tokenize(target: String): List<Token> {
        val ctx = TokenizerContext(target, pattern)
        while (true) {
            if (ctx.pos == ctx.target.length) break
            if (tryMatch(ctx)) continue
        }
        if (ctx.pos != target.length) addToTokens(ctx, target.substring(ctx.pos))
        ctx.tokens.add(EofToken.getToken("", ctx.line, null))
        return ctx.tokens
    }

    private class TokenizerContext(val target: String, pattern: Pattern) {
        val tokens = ArrayList<Token>()
        val matcher: Matcher = pattern.matcher(target)

        var pos = 0
        var line = 1

        val prev get() = get(-1)
        val current get() = target[pos]
        val next get() = get(1)

        operator fun get(offset: Int) = target.getOrNull(pos + offset)
    }

    private fun tryIgnoreToken(ctx: TokenizerContext): Boolean {
        if (ctx.current != '\r' && ctx.current != '\n' && ctx.current != '\t') return false

        if (isNewLine(ctx)) ctx.line++
        ctx.pos++

        return true
    }

    private fun isNewLine(ctx: TokenizerContext): Boolean {
        return ctx.current == '\r' && ctx.next == '\n'
    }

    private fun trySmartToken(ctx: TokenizerContext): Boolean {
        val m = ctx.matcher
        if (!m.find(ctx.pos)) return false

        if (ctx.pos != m.start()) addToTokens(ctx, ctx.target.substring(ctx.pos, m.start()))
        addToTokens(ctx, m.group())
        ctx.pos = m.end()
        return true
    }

    private val escapeCharacters = mapOf(
            't' to '\t',
            'b' to '\b',
            'n' to '\n',
            'r' to '\r',
            '"' to '\"',
            '\\' to '\\',
    )

    private fun tryStringLiteral(ctx: TokenizerContext): Boolean {
        if (ctx.current != '"') return false

        val builder = StringBuilder()
        ctx.pos++
        while (ctx.current != '"') {
            val toAdd = if (ctx.current == '\\') {
                ctx.pos++
                escapeCharacters[ctx.current] ?: ctx.current
            } else {
                ctx.current
            }
            builder.append(toAdd)
            ctx.pos++
        }
        ctx.tokens.add(StringToken.getToken(builder.toString(), ctx.line, builder.toString()))
        ctx.pos++

        return true
    }

    private fun trySingleLineComment(ctx: TokenizerContext): Boolean {
        if (ctx.current != '/' || ctx.next != '/') return false

        ctx.pos += 2
        while (!isNewLine(ctx)) ctx.pos++
        ctx.pos += 2
        ctx.line++

        return true
    }

    private fun tryMultiLineComment(ctx: TokenizerContext): Boolean {
        if (ctx.current != '/' || ctx.next != '*') return false

        ctx.pos += 2
        while (ctx.current != '*' || ctx.next != '/') {
            if (isNewLine(ctx)) {
                ctx.pos += 2
                ctx.line++
            } else ctx.pos++
        }
        ctx.pos += 2
        return true
    }

    private fun getKeyword(ctx: TokenizerContext, lexeme: String): Boolean {
        val keyword = keywordTokens.find { it.matches(lexeme) } ?: return false
        ctx.tokens.add(keyword.getToken(lexeme, ctx.line, null))
        return true
    }

    private fun addToTokens(ctx: TokenizerContext, lexeme: String) {
        val trimmed = lexeme.trim { it <= ' ' }
        if (trimmed.isEmpty()) return

        //find keywords first
        if (getKeyword(ctx, trimmed)) return

        val token = tokenReaders.find { it.matches(trimmed) } ?: return
        ctx.tokens.add(token.getToken(trimmed, ctx.line, null))
    }
}
package me.jaden.token

import compile.api.Token
import compile.api.TokenType

object StringToken : TokenType {
    override fun matches(lexeme: String) = false
    override fun getToken(lexeme: String, line: Int, literal: Any?) = Token(this, lexeme, line, lexeme)
}
package me.jaden.token

import compile.api.Expression
import compile.api.TokenType
import me.jaden.expression.*

object Operators : TokenProvider() {
    val Eq = token("=", binary = ::BinaryAssign)
    val PlusEq = token("+=", binary = ::BinaryAddAssign)
    val MinusEq = token("-=", binary = ::BinarySubAssign)
    val StarEq = token("*=", binary = ::BinaryMulAssign)
    val SlashEq = token("/=", binary = ::BinaryDivAssign)
    val ModuloEq = token("%=", binary = ::BinaryRemAssign)
    val assignment = arrayOf(Eq, PlusEq, MinusEq, StarEq, SlashEq, ModuloEq)


    val Or = token("||", binary = ::BinaryOr)
    val disjunction = arrayOf(Or)


    val And = token("&&", binary = ::BinaryAnd)
    val conjunction = arrayOf(And)


    val EqEq = token("==", binary = ::BinaryEqual)
    val BangEq = token("!=", binary = ::BinaryNotEqual)
    val equality = arrayOf(EqEq, BangEq)


    val LT = token("<", binary = ::BinaryLessThan)
    val LE = token("<=", binary = ::BinaryLessEqual)
    val GT = token(">=", binary = ::BinaryGreaterEqual)
    val GE = token(">", binary = ::BinaryGreaterThan)
    val comparison = arrayOf(LT, LE, GE, GT)


    val Plus = token("+", binary = ::BinaryAdd, unary = ::UnaryPlus)
    val Minus = token("-", binary = ::BinarySub, unary = ::UnaryMinus)
    val additive = arrayOf(Plus, Minus)


    val Star = token("*", binary = ::BinaryMul)
    val Slash = token("/", binary = ::BinaryDiv)
    val Modulo = token("%", binary = ::BinaryRem)
    val multiplicative = arrayOf(Star, Slash, Modulo)


    val PlusPlus = token("++", unary = ::PrePlusPlus)
    val MinusMinus = token("--", unary = ::PreMinusMinus)
    val Excl = token("!", unary = ::UnaryNot)
    val prefixUnary = arrayOf(PlusPlus, MinusMinus, Plus, Minus, Excl)



    val Elvis = token("?:", binary = ::ElvisExpression)

    val Colon = token(":")
    val Semicolon = token(";")
    val Comma = token(",")

    val Dot = token(".")
    val QuestionDot = token("?.")

    val DotDot = token("..")

    val Arrow = token("->")



    val LBrace = token("{")
    val RBrace = token("}")
    val LParen = token("(")
    val RParen = token(")")
    val LBrack = token("[")
    val RBrack = token("]")



    private fun token(lexeme: String, binary: ((Expression, Expression) -> Expression)? = null, unary: ((Expression) -> Expression)? = null): TokenType {
        val ret = AbstractToken(lexeme, binary = binary, unary = unary)
        addToken(ret)
        return ret
    }
}

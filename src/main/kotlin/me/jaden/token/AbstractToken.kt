package me.jaden.token

import compile.api.Expression
import compile.api.Token
import compile.api.TokenType

class AbstractToken(
        private val delimiter: String,
        override val priority: Int = 0,
        private val binary: ((Expression, Expression) -> Expression)? = null,
        private val unary: ((Expression) -> Expression)? = null
) : TokenType {
    override fun toString() = super.toString() + ": $delimiter"
    override fun addDelimiter(delimiters: MutableCollection<String>) {
        delimiters.add(delimiter)
    }
    override fun matches(lexeme: String) = lexeme == delimiter
    override fun getToken(lexeme: String, line: Int, literal: Any?) = Token(this, lexeme, line, literal)
    override fun binaryExpression(left: Expression, right: Expression): Expression {
        return binary?.invoke(left, right) ?: super.binaryExpression(left, right)
    }

    override fun unaryExpression(value: Expression): Expression {
        return unary?.invoke(value) ?: super.unaryExpression(value)
    }
}
package me.jaden.token

import compile.api.TokenType

object Keywords : TokenProvider() {
    val Function = keyword("function")
    val If = keyword("if")
    val Return = keyword("return")
    val Null = keyword("null")
    val False = keyword("false")
    val True = keyword("true")

    val Class = keyword("class")
    val Else = keyword("else")
    val For = keyword("for")
    val Var = keyword("var")
    val Val = keyword("val")
    val While = keyword("while")
    val Do = keyword("do")

    val In = keyword("in")
    val Step = keyword("step")

    private fun keyword(lexeme: String): TokenType {
        val ret = AbstractToken(lexeme, -1)
        addToken(ret)
        return ret
    }
}

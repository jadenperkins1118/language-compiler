package me.jaden.token

import compile.api.TokenType

object IdentifierToken : TokenType {
    override val priority = 5
    override fun matches(lexeme: String): Boolean {
        return lexeme.matches("[_a-zA-Z][0-9a-zA-Z]*".toRegex())
    }
}
package me.jaden.token

import compile.api.Token
import compile.api.TokenType

object NumberToken : TokenType {
    override fun matches(lexeme: String) =  lexeme.matches("0|[1-9][0-9]*(.[0-9]+)?".toRegex())
    override fun getToken(lexeme: String, line: Int, literal: Any?) = Token(this, lexeme, line, lexeme.toDouble())
}
package me.jaden.token

import compile.api.TokenType

abstract class TokenProvider {
    private val tokens = ArrayList<TokenType>()
    fun tokens() = tokens.toTypedArray()

    fun addToken(type: TokenType) {
        tokens.add(type)
    }
}
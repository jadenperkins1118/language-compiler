package me.jaden.expression

import com.opcodes.*
import com.vm.VMFunction
import compile.api.Expression

abstract class BinaryPlusOp(private val identifier: Expression, private val right: Expression, private val output: VMFunction.() -> Unit) : Expression {
    override fun outputCode(method: VMFunction) {
        identifier.preAssign(method)
        identifier.compoundPreAssign(method)
        right.outputCode(method)
        method.output()
        identifier.assign(method)
    }
}
data class BinaryAddAssign(private val identifier: Expression, private val right: Expression) : BinaryPlusOp(identifier, right, VMFunction::DADD)
data class BinarySubAssign(private val identifier: Expression, private val right: Expression) : BinaryPlusOp(identifier, right, VMFunction::DSUB)
data class BinaryMulAssign(private val identifier: Expression, private val right: Expression) : BinaryPlusOp(identifier, right, VMFunction::DMUL)
data class BinaryDivAssign(private val identifier: Expression, private val right: Expression) : BinaryPlusOp(identifier, right, VMFunction::DDIV)
data class BinaryRemAssign(private val identifier: Expression, private val right: Expression) : BinaryPlusOp(identifier, right, VMFunction::DREM)
data class BinaryAssign(private val identifier: Expression, private val right: Expression) : Expression {
    override fun outputCode(method: VMFunction) {
        identifier.preAssign(method)
        right.outputCode(method)
        identifier.assign(method)
    }
}
package me.jaden.expression

import com.opcodes.GOTO
import com.opcodes.IFNE
import com.opcodes.PUSH
import com.vm.Label
import com.vm.VMFunction
import compile.api.Expression

abstract class BinaryLogic(private val left: Expression, private val right: Expression, private val value: Boolean) : Expression {
    override fun outputCode(method: VMFunction) {
        val label1 = Label()
        val label2 = Label()

        left.outputCode(method)

        method.IFNE(label1)
        method.PUSH(value)
        method.GOTO(label2)

        method.label(label1)
        right.outputCode(method)

        method.label(label2)
    }
}

data class BinaryAnd(private val left: Expression, private val right: Expression) : BinaryLogic(left, right, false)
data class BinaryOr(private val left: Expression, private val right: Expression) : BinaryLogic(left, right, true)
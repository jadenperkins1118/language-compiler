package me.jaden.expression

import com.opcodes.*
import com.vm.VMFunction
import compile.api.Expression
import compile.api.Parser
import compile.api.Token
import me.jaden.parser.doWhileMatch
import me.jaden.token.*

data class LiteralExpression(private val value: Any?) : Expression {
    override fun outputCode(method: VMFunction) = method.PUSH(value)

    companion object {
        fun create(parser: Parser): Expression? {
            val value = when {
                parser.match(Keywords.True) -> true
                parser.match(Keywords.False) -> false
                parser.match(Keywords.Null) -> null
                parser.match(StringToken) -> parser.prev().literal as String
                parser.match(NumberToken) -> (parser.prev().literal as Number).toDouble()
                else -> return null
            }
            return LiteralExpression(value)
        }
    }
}

data class ArrayLiteral(private val values: List<Expression>) : Expression {
    override fun outputCode(method: VMFunction) {
        method.PUSH(values.size)
        method.NEWARRAY(12)
        for ((index, value) in values.withIndex()) {
            method.DUP()
            method.PUSH(index)
            value.outputCode(method)
            method.ASTORE()
        }
    }
    companion object {
        fun create(parser: Parser): Expression? {
            if (!parser.match(Operators.LBrack)) return null

            val values = doWhileMatch(parser, Operators.Comma, Operators.RBrack, ExpressionHelper::expression)
            return ArrayLiteral(values)
        }
    }
}

data class ObjectLiteralExpression(private val properties: List<Expression>) : Expression {
    override fun outputCode(method: VMFunction) {
        method.NEW()
        properties.forEach { it.outputCode(method) }
    }

    companion object {
        fun create(parser: Parser): Expression? {
            if (!parser.match(Operators.LBrace)) return null

            val properties = doWhileMatch(parser, Operators.Comma, Operators.RBrace, ::propAssign)
            return ObjectLiteralExpression(properties)
        }

        private fun propAssign(parser: Parser): Expression {
            ComputedPropertyExpression.create(parser)?.let { return it }
            StringPropertyExpression.create(parser)?.let { return it }
            PropertyLiteralExpression.create(parser)?.let { return it }
            return PropertyCopyExpression(parser.expect(IdentifierToken))
        }
    }
}

data class PropertyLiteralExpression(private val property: Token, private val value: Expression) : Expression {
    override fun outputCode(method: VMFunction) {
        method.DUP()
        value.outputCode(method)
        method.PUT(property.lexeme)
    }

    companion object {
        fun create(parser: Parser): Expression? {
            if (parser.curr().type !== IdentifierToken) return null
            if (parser.next().type !== Operators.Colon) return null

            val identifier = parser.expect(IdentifierToken)
            parser.expect(Operators.Colon)

            return PropertyLiteralExpression(identifier, ExpressionHelper.expression(parser))
        }
    }
}

data class PropertyCopyExpression(private val identifier: Token) : Expression {
    override fun outputCode(method: VMFunction) {
        method.DUP()
        method.LOAD(identifier.lexeme)
        method.PUT(identifier.lexeme)
    }
}

data class ComputedPropertyExpression(private val property: Expression, private val value: Expression) : Expression {
    override fun outputCode(method: VMFunction) {
        method.DUP()
        value.outputCode(method)
        property.outputCode(method)
        method.PUTDYNAMIC()
    }

    companion object {
        fun create(parser: Parser): Expression? {
            if (!parser.match(Operators.LBrack)) return null

            val property = ExpressionHelper.expression(parser)
            parser.expect(Operators.RBrack)
            parser.expect(Operators.Colon)
            return ComputedPropertyExpression(property, ExpressionHelper.expression(parser))
        }
    }
}

data class StringPropertyExpression(private val property: Expression, private val value: Expression) : Expression {
    override fun outputCode(method: VMFunction) {
        method.DUP()
        value.outputCode(method)
        property.outputCode(method)
        method.PUTDYNAMIC()
    }
    companion object {
        fun create(parser: Parser): Expression? {
            val literal = LiteralExpression.create(parser) ?: return null

            parser.expect(Operators.Colon)
            return StringPropertyExpression(literal, ExpressionHelper.expression(parser))
        }
    }
}
package me.jaden.expression

import com.opcodes.DADD
import com.opcodes.DSUB
import com.opcodes.PUSH
import com.vm.VMFunction
import compile.api.Expression
import compile.api.Parser
import me.jaden.token.Operators

abstract class Nudge(private val identifier: Expression, private val pre: Boolean, private val output: VMFunction.() -> Unit) : Expression {
    override fun outputCode(method: VMFunction) {
        identifier.outputCode(method)
        if (pre) pre(method) else post(method)
        identifier.assign(method)
    }

    private fun pre(method: VMFunction) {
        method.PUSH(1.0)
        method.output()
    }

    private fun post(method: VMFunction) {
        method.PUSH(1.0)
        method.output()
    }
}

data class PrePlusPlus(private val value: Expression) : Nudge(value, true, VMFunction::DADD)
data class PreMinusMinus(private val value: Expression) : Nudge(value, true, VMFunction::DSUB)

data class PostPlusPlus(private val identifier: Expression) : Nudge(identifier, false, VMFunction::DADD) {
    companion object {
        fun create(parser: Parser, left: Expression): Expression? {
            if (!parser.match(Operators.PlusPlus)) return null
            return PostPlusPlus(left)
        }
    }
}
data class PostMinusMinus(private val value: Expression) : Nudge(value, false, VMFunction::DSUB) {
    companion object {
        fun create(parser: Parser, left: Expression): Expression? {
            if (!parser.match(Operators.MinusMinus)) return null
            return PostMinusMinus(left)
        }
    }
}


package me.jaden.expression

import com.opcodes.ARETURN
import com.opcodes.GOTO
import com.opcodes.IFEQ
import com.opcodes.INVOKE
import com.vm.Label
import com.vm.VMFunction
import compile.api.Expression
import compile.api.Parser
import me.jaden.parser.doWhileMatch
import me.jaden.token.Keywords
import me.jaden.token.Operators


data class ValueArgumentsExpression(private val identifier: Expression, private val arguments: List<Expression>) : Expression {
    override fun outputCode(method: VMFunction) {
        identifier.outputCode(method)
        arguments.forEach { it.outputCode(method) }
        method.INVOKE()
    }

    companion object {
        fun create(parser: Parser, left: Expression): Expression? {
            if (!parser.match(Operators.LParen)) return null

            val args = doWhileMatch(parser, Operators.Comma, Operators.RParen, ExpressionHelper::expression)
            return ValueArgumentsExpression(left, args)
        }
    }
}

data class ParenthesizedExpression(private val expression: Expression) : Expression {
    override fun outputCode(method: VMFunction) = expression.outputCode(method)

    companion object {
        fun create(parser: Parser): Expression? {
            if (!parser.match(Operators.LParen)) return null
            val exp = ExpressionHelper.expression(parser)
            parser.expect(Operators.RParen)

            return ParenthesizedExpression(exp)
        }
    }
}

data class IfExpression(private val condition: Expression, private val ifBody: Expression, private val elseBody: Expression?) : Expression {
    override fun outputCode(method: VMFunction) {
        val end = Label()

        val falseLabel = Label()

        condition.outputCode(method)
        method.IFEQ(falseLabel)
        ifBody.outputCode(method)
        method.GOTO(end)

        method.label(falseLabel)
        elseBody?.outputCode(method)
        method.label(end)
    }

    companion object {
        fun create(parser: Parser): Expression? {
            if (!parser.match(Keywords.If)) return null
            parser.expect(Operators.LParen)

            val condition = ExpressionHelper.expression(parser)
            parser.expect(Operators.RParen)

            val ifBody = ExpressionHelper.controlStructureBody(parser)
            val elseBody = if (parser.match(Keywords.Else)) ExpressionHelper.controlStructureBody(parser) else null

            return IfExpression(condition, ifBody, elseBody)
        }
    }
}

data class ReturnExpression(private val value: Expression) : Expression {
    override fun outputCode(method: VMFunction) {
        value.outputCode(method)
        method.ARETURN()
    }

    companion object {
        fun create(parser: Parser): Expression? {
            if (!parser.match(Keywords.Return)) return null
            return ReturnExpression(ExpressionHelper.expression(parser))
        }
    }
}

data class ExpressionList(private val expressions: List<Expression>) : Expression {
    override fun outputCode(method: VMFunction) {
        expressions.forEach { it.outputCode(method) }
    }

    companion object {
        fun create(parser: Parser): Expression? {
            if (!parser.match(Operators.LBrace)) return null

            val statements = ArrayList<Expression>()
            while (!parser.match(Operators.RBrace)) statements.add(ExpressionHelper.statement(parser))

            return ExpressionList(statements)
        }
    }
}


package me.jaden.expression

import com.opcodes.*
import com.vm.Label
import com.vm.VMFunction
import compile.api.Expression
import compile.api.Parser
import compile.api.Token
import me.jaden.parser.expectParenthesis
import me.jaden.token.IdentifierToken
import me.jaden.token.Keywords
import me.jaden.token.Operators

class WhileExpression(private val condition: Expression, private val body: Expression) : Expression {
    override fun outputCode(method: VMFunction) {
        val start = Label()
        val end = Label()

        method.label(start)
        condition.outputCode(method)

        method.IFEQ(end)
        body.outputCode(method)
        method.GOTO(start)

        method.label(end)
    }

    companion object {
        fun create(parser: Parser): Expression? {
            if (!parser.match(Keywords.While)) return null

            val condition = parser.expectParenthesis(ExpressionHelper::expression)
            val body = ExpressionHelper.controlStructureBody(parser)
            return WhileExpression(condition, body)
        }
    }
}

class DoWhileExpression(private val body: Expression, private val condition: Expression) : Expression {
    override fun outputCode(method: VMFunction) {
        val start = Label()

        method.label(start)

        body.outputCode(method)

        condition.outputCode(method)
        method.IFNE(start)
    }

    companion object {
        fun create(parser: Parser): Expression? {
            if (!parser.match(Keywords.Do)) return null

            val body = ExpressionHelper.controlStructureBody(parser)
            parser.expect(Keywords.While)
            val condition = parser.expectParenthesis(ExpressionHelper::expression)
            return DoWhileExpression(body, condition)
        }
    }
}

class ForExpression(private val varName: Token, private val first: Expression, private val final: Expression, private val step: Expression?, private val body: Expression) : Expression {
    override fun outputCode(method: VMFunction) {
        val start = Label()
        val end = Label()
        val decreasing = Label()
        val bodyLabel = Label()

        first.outputCode(method)                        //expr      [init]
        method.STORE(varName.lexeme)                    //store     []

        method.label(start)                             //--start
        method.LOAD(varName.lexeme)                     //load      [i]
        final.outputCode(method)                        //expr      [i, final]
        method.DCMPG()                                  //dcmpg     [result]
        step?.outputCode(method) ?: method.PUSH(1.0)    //expr?push [result, incr]
        method.PUSH(0.0)                                //push      [result, incr, 0.0]
        method.DCMPG()                                  //dcmpg     [result, target]
        method.IFLT(decreasing)                         //iflt      [result] -> decreasing

        method.IFGT(end)                                //ifgt      [] -> end
        method.GOTO(bodyLabel)                          //goto      [] -> bodyLabel

        method.label(decreasing)                        //--decreasing
        method.IFLT(end)                                //iflt      [] -> end

        method.label(bodyLabel)                         //--bodyLabel
        body.outputCode(method)                         //ops*      []
        method.LOAD(varName.lexeme)                     //load      [i]
        step?.outputCode(method) ?: method.PUSH(1.0)    //expr?push [i, incr]
        method.DADD()                                   //dadd      [result]
        method.STORE(varName.lexeme)                    //store     []
        method.GOTO(start)                              //goto      [] -> start

        method.label(end)                               //--end
    }

    companion object {
        fun create(parser: Parser): Expression? {
            if (!parser.match(Keywords.For)) return null

            parser.expect(Operators.LParen)
            val varName = parser.expect(IdentifierToken)
            parser.expect(Keywords.In)

            val min = ExpressionHelper.expression(parser)
            parser.expect(Operators.DotDot)
            val max = ExpressionHelper.expression(parser)

            val step = if (parser.match(Keywords.Step)) ExpressionHelper.expression(parser) else null
            parser.expect(Operators.RParen)

            val body = ExpressionHelper.controlStructureBody(parser)

            return ForExpression(varName, min, max, step, body)
        }
    }
}

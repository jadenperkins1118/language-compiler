package me.jaden.expression

import com.opcodes.*
import com.vm.Label
import com.vm.VMFunction
import compile.api.Expression

abstract class BinaryCompare(private val left: Expression, private val right: Expression, private val compare: VMFunction.(Label) -> Unit) : Expression {
    override fun outputCode(method: VMFunction) {
        val label1 = Label()
        val label2 = Label()

        left.outputCode(method)
        right.outputCode(method)
        method.DCMPL()
        method.compare(label1)
        method.PUSH(0)
        method.GOTO(label2)
        method.label(label1)
        method.PUSH(1)
        method.label(label2)
    }
}

data class BinaryLessThan(private val left: Expression, private val right: Expression) : BinaryCompare(left, right, VMFunction::IFLT)
data class BinaryLessEqual(private val left: Expression, private val right: Expression) : BinaryCompare(left, right, VMFunction::IFLE)
data class BinaryGreaterThan(private val left: Expression, private val right: Expression) : BinaryCompare(left, right, VMFunction::IFGT)
data class BinaryGreaterEqual(private val left: Expression, private val right: Expression) : BinaryCompare(left, right, VMFunction::IFGE)
data class BinaryEqual(private val left: Expression, private val right: Expression) : BinaryCompare(left, right, VMFunction::IFEQ)
data class BinaryNotEqual(private val left: Expression, private val right: Expression) : BinaryCompare(left, right, VMFunction::IFNE)
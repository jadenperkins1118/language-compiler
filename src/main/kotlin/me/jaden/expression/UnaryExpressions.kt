package me.jaden.expression

import com.opcodes.DNEG
import com.opcodes.ISUB
import com.opcodes.PUSH
import com.vm.VMFunction
import compile.api.Expression

data class UnaryPlus(private val value: Expression) : Expression {
    override fun outputCode(method: VMFunction) = value.outputCode(method)
}

data class UnaryMinus(private val value: Expression) : Expression {
    override fun outputCode(method: VMFunction) {
        value.outputCode(method)
        method.DNEG()
    }
}
data class UnaryNot(private val value: Expression) : Expression {
    override fun outputCode(method: VMFunction) {
        method.PUSH(1)
        value.outputCode(method)
        method.ISUB()
    }
}
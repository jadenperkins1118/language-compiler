package me.jaden.expression

import compile.api.Expression
import compile.api.Parser
import me.jaden.parser.decideExpression
import me.jaden.token.IdentifierToken
import me.jaden.token.Operators

object ExpressionHelper {
    fun statement(parser: Parser): Expression {
        return declaration(parser) ?: expression(parser)
    }

    private fun declaration(parser: Parser): Expression? {
        VarDeclaration.create(parser)?.let { return it }
        ValDeclaration.create(parser)?.let { return it }
        FunctionDeclaration.create(parser)?.let { return it }

        return null
    }

    fun controlStructureBody(parser: Parser): Expression {
        return ExpressionList.create(parser) ?: expression(parser)
    }

    fun expression(parser: Parser) = decideExpression(parser, Operators.assignment, ::disjunction)
    private fun disjunction(parser: Parser) = decideExpression(parser, Operators.disjunction, ::conjunction)
    private fun conjunction(parser: Parser) = decideExpression(parser, Operators.conjunction, ::equalityComparison)
    private fun equalityComparison(parser: Parser) = decideExpression(parser, Operators.equality, ::comparison)
    private fun comparison(parser: Parser) = decideExpression(parser, Operators.comparison, ::elvis)
    private fun elvis(parser: Parser) = decideExpression(parser, arrayOf(Operators.Elvis), ::additive)
    private fun additive(parser: Parser) = decideExpression(parser, Operators.additive, ::multiplicative)
    private fun multiplicative(parser: Parser) = decideExpression(parser, Operators.multiplicative, ::prefixUnary)

    private fun prefixUnary(parser: Parser): Expression {
        if (parser.curr().type in Operators.prefixUnary) {
            val token = parser.advance()
            return token.type.unaryExpression(postfixUnary(parser))
        }

        return postfixUnary(parser)
    }

    private fun postfixUnary(parser: Parser): Expression {
        var left = atomic(parser)

        fun <R : Expression> doAssign(block: Parser.(Expression) -> R?): Boolean {
            return parser.block(left)?.let { left = it } != null
        }

        while (true) {
            if (doAssign(PostPlusPlus::create)) continue
            if (doAssign(PostMinusMinus::create)) continue
            if (doAssign(ValueArgumentsExpression::create)) continue
            if (doAssign(ArrayAccessExpression::create)) continue
            if (doAssign(MemberAccessExpression::create)) continue
            if (doAssign(SafeMemberAccessExpression::create)) continue

            break
        }

        return left
    }

    private fun atomic(parser: Parser): Expression {
        ObjectLiteralExpression.create(parser)?.let { return it }
        ArrayLiteral.create(parser)?.let { return it }
        AnonymousFunctionExpression.create(parser)?.let { return it }

        IfExpression.create(parser)?.let { return it }

        ReturnExpression.create(parser)?.let { return it }
//        ContinueExpression.create(parser)?.let { return it }
//        BreakExpression.create(parser)?.let { return it }

        ForExpression.create(parser)?.let { return it }
        WhileExpression.create(parser)?.let { return it }
        DoWhileExpression.create(parser)?.let { return it }

        LiteralExpression.create(parser)?.let { return it }

        ParenthesizedExpression.create(parser)?.let { return it }

        return SimpleIdentifierExpression(parser.expect(IdentifierToken))
    }
}
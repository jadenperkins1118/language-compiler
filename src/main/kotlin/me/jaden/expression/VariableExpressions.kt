package me.jaden.expression

import com.opcodes.PUSH
import com.vm.VMFunction
import compile.api.Expression
import compile.api.Parser
import me.jaden.token.IdentifierToken
import me.jaden.token.Keywords
import me.jaden.token.Operators

data class VarDeclaration(private val identifier: Expression) : Expression {
    override fun outputCode(method: VMFunction) {
        method.PUSH(null)
        identifier.assign(method)
    }

    companion object {
        fun create(parser: Parser): Expression? {
            if (!parser.match(Keywords.Var)) return null

            val id = SimpleIdentifierExpression(parser.expect(IdentifierToken))
            if (parser.match(Operators.Eq)) return BinaryAssign(id, ExpressionHelper.expression(parser))
            return VarDeclaration(id)
        }
    }
}

data class ValDeclaration(private val identifier: Expression) : Expression {
    override fun outputCode(method: VMFunction) {
        method.PUSH(null)
        identifier.assign(method)
    }

    companion object {
        fun create(parser: Parser): Expression? {
            if (!parser.match(Keywords.Val)) return null

            val id = SimpleIdentifierExpression(parser.expect(IdentifierToken))
            if (parser.match(Operators.Eq)) return BinaryAssign(id, ExpressionHelper.expression(parser))
            return ValDeclaration(id)
        }
    }
}
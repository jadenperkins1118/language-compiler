package me.jaden.expression

import com.opcodes.*
import com.vm.Label
import com.vm.VMFunction
import compile.api.Expression

abstract class BinaryExpression(private val left: Expression, private val right: Expression, val output: VMFunction.() -> Unit) : Expression {
    override fun outputCode(method: VMFunction) {
        left.outputCode(method)
        right.outputCode(method)
        method.output()
    }
}
data class BinaryAdd(private val left: Expression, private val right: Expression) : BinaryExpression(left, right, VMFunction::DADD)
data class BinarySub(private val left: Expression, private val right: Expression) : BinaryExpression(left, right, VMFunction::DSUB)
data class BinaryMul(private val left: Expression, private val right: Expression) : BinaryExpression(left, right, VMFunction::DMUL)
data class BinaryDiv(private val left: Expression, private val right: Expression) : BinaryExpression(left, right, VMFunction::DDIV)
data class BinaryRem(private val left: Expression, private val right: Expression) : BinaryExpression(left, right, VMFunction::DREM)

data class ElvisExpression(private val left: Expression, private val right: Expression) : Expression {
    override fun outputCode(method: VMFunction) {
        val notNull = Label()
        left.outputCode(method)
        method.DUP()
        method.IFNONNULL(notNull)
        method.POP()
        right.outputCode(method)
        method.label(notNull)
    }
}
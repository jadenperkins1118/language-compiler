package me.jaden.expression

import com.opcodes.*
import com.vm.Label
import com.vm.VMFunction
import compile.api.Expression
import compile.api.Parser
import compile.api.Token
import me.jaden.token.IdentifierToken
import me.jaden.token.Operators

data class MemberAccessExpression(private val target: Expression, private val member: Expression) : Expression {
    override fun compoundPreAssign(method: VMFunction) {
        method.DUP()
        member.outputCode(method)
    }
    override fun preAssign(method: VMFunction) = target.outputCode(method)
    override fun outputCode(method: VMFunction) {
        target.outputCode(method)
        member.outputCode(method)
    }
    override fun assign(method: VMFunction) = member.assign(method)

    companion object {
        fun create(parser: Parser, left: Expression): Expression? {
            if (!parser.match(Operators.Dot)) return null

            val expression = MemberIdentifierExpression(parser.expect(IdentifierToken))
            return MemberAccessExpression(left, expression)
        }
    }
}

data class SafeMemberAccessExpression(private val target: Expression, private val member: Expression) : Expression {
    override fun outputCode(method: VMFunction) {
        val end = Label()

        target.outputCode(method)
        method.DUP()
        method.IFNULL(end)
        member.outputCode(method)
        method.label(end)
    }

    companion object {
        fun create(parser: Parser, left: Expression): Expression? {
            if (!parser.match(Operators.QuestionDot)) return null

            val expression = MemberIdentifierExpression(parser.expect(IdentifierToken))
            return SafeMemberAccessExpression(left, expression)
        }
    }
}

data class ArrayAccessExpression(val identifier: Expression, val index: Expression) : Expression {
    override fun compoundPreAssign(method: VMFunction) {
        outputCode(method)
    }

    override fun preAssign(method: VMFunction) {
        identifier.outputCode(method)
        index.outputCode(method)
        method.D2I()
    }

    override fun outputCode(method: VMFunction) {
        preAssign(method)
        method.ALOAD()
    }

    override fun assign(method: VMFunction) = method.ASTORE()

    companion object {
        fun create(parser: Parser, left: Expression): Expression? {
            if (!parser.match(Operators.LBrack)) return null

            val index = ExpressionHelper.expression(parser)
            parser.expect(Operators.RBrack)
            return ArrayAccessExpression(left, index)
        }
    }
}

data class MemberIdentifierExpression(val identifier: Token) : Expression {
    override fun outputCode(method: VMFunction) = method.GET(identifier.lexeme)
    override fun assign(method: VMFunction) = method.PUT(identifier.lexeme)
}

data class SimpleIdentifierExpression(val identifier: Token) : Expression {
    override fun outputCode(method: VMFunction) = method.LOAD(identifier.lexeme)
    override fun assign(method: VMFunction) = method.STORE(identifier.lexeme)
}
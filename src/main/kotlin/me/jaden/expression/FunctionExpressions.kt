package me.jaden.expression

import com.opcodes.PUSH
import com.opcodes.RETURN
import com.opcodes.STORE
import com.vm.VMFunction
import com.vm.instruction.ReturnInstruction
import compile.api.Expression
import compile.api.Parser
import compile.api.Token
import me.jaden.parser.doWhileMatch
import me.jaden.token.IdentifierToken
import me.jaden.token.Keywords
import me.jaden.token.Operators

data class FunctionDeclaration(private val identifier: Token, private val params: List<Token>, private val body: Expression) : Expression {
    override fun outputCode(method: VMFunction) {
        val functionName = identifier.lexeme

        val function = VMFunction(functionName, params.map { it.lexeme }.toTypedArray()) {
            body.outputCode(this)
            if (peek() !is ReturnInstruction) RETURN()
        }

        method.PUSH(function)
        method.STORE(functionName)
    }

    companion object {
        fun create(parser: Parser): Expression? {
            if (!parser.match(Keywords.Function)) return null

            val id = parser.expect(IdentifierToken)
            val params = functionValueParameters(parser)
            val body = functionBody(parser)

            return FunctionDeclaration(id, params, body)
        }
    }
}

private fun functionValueParameters(parser: Parser): List<Token> {
    parser.expect(Operators.LParen)
    return doWhileMatch(parser, Operators.Comma, Operators.RParen) { parser.expect(IdentifierToken) }
}

private fun functionBody(parser: Parser): Expression {
    ExpressionList.create(parser)?.let { return it }

    parser.expect(Operators.Eq)
    return ExpressionHelper.expression(parser)
}


data class AnonymousFunctionExpression(private val params: List<Token>, private val body: Expression) : Expression {
    override fun outputCode(method: VMFunction) {
        val newMethod = VMFunction("<anon>", params.map { it.lexeme }.toTypedArray()) {
            body.outputCode(this)
            if (peek() !is ReturnInstruction) RETURN()
        }
        method.PUSH(newMethod)
    }

    companion object {
        fun create(parser: Parser): Expression? {
            if (parser.match(Keywords.Function)) {
                val params = functionValueParameters(parser)
                val body = functionBody(parser)
                return AnonymousFunctionExpression(params, body)
            }

            if (parser.curr().type === Operators.LParen) {
                val params = functionValueParameters(parser)
                parser.expect(Operators.Arrow)
                val body = ExpressionHelper.controlStructureBody(parser)
                return AnonymousFunctionExpression(params, body)
            }

            return null
        }
    }
}
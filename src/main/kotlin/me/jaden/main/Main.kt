package me.jaden.main

import com.opcodes.RETURN
import com.vm.ScriptObjectWrapper
import com.vm.VM
import com.vm.VMFunction
import com.vm.instruction.ReturnInstruction
import com.vm.strategy.SimpleExecutionStrategy
import com.vm.symbol.GlobalSymbols
import me.jaden.parser.JavaScriptParser
import me.jaden.token.Tokens
import java.nio.file.Files
import java.nio.file.Paths
import javax.script.ScriptEngineManager

fun main() {
    val script = readScript("./scripts/Test.js")
    println(script)

    for (i in 0 until 10000) {
        10 + 20
    }

    val results = HashMap<String, Long>()
    for (i in 0 until 100) {
        val myTime = time { usingMyEngine(script) }.second
        val nhTime = time { usingNashorn(script) }.second

        results["KL"] = (results["KL"] ?: 0L) + myTime
        results["NH"] = (results["NH"] ?: 0L) + nhTime
    }

    println(results.getValue("NH").toDouble() / results.getValue("KL").toDouble())

    results.forEach { (k, v) -> println("$k result: $v") }
}

private fun usingMyEngine(script: String) {
    val tokens = Tokens.tokenize(script)

    val parser = JavaScriptParser(tokens)
    val expression = parser.parse()

    val main = VMFunction("main", arrayOf("console")) {
        expression.outputCode(this)
        if (peek() !is ReturnInstruction) RETURN()
    }

    val vm = VM(mapOf("main" to main), SimpleExecutionStrategy)

    val result = vm.execute(GlobalSymbols.from(mapOf("console" to ScriptObjectWrapper(Console))))
    if (result !== Unit) println(result)
}

private val manager = ScriptEngineManager()

private fun usingNashorn(script: String) {
    val engine = manager.getEngineByName("nashorn")
    engine.eval(script)
}

private inline fun <R> time(block: () -> R): Pair<R, Long> {
    val start = System.nanoTime()
    val result = block()
    val end = System.nanoTime()
    return result to (end - start)
}

private object Console {
    fun log(msg: String) = println(msg)
    fun err(msg: String) = System.err.println(msg)
}

private fun readScript(path: String): String {
    val bytes = Files.readAllBytes(Paths.get(path))
    return String(bytes)
}
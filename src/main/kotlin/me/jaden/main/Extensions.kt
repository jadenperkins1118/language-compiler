package me.jaden.main

fun error(line: Int, message: String) = report(line, "", message)
fun report(line: Int, where: String, message: String) = System.err.println("[line $line] Error $where: $message")